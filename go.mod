module ssh

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/sftp v1.13.0
	github.com/pytool/ssh v0.0.0-20190312091242-5aaea5918db7
	github.com/yeka/zip v0.0.0-20180914125537-d046722c6feb
	golang.org/x/crypto v0.0.0-20210314154223-e6e6c4f2bb5b
)
