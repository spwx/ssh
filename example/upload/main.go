package main

import (
	"github.com/pytool/ssh"
)

func main() {

	client, err := ssh.NewClient("118.24.78.141", "22", "ubuntu", "")
	if err != nil {
		panic(err)
	}
	defer client.Close()
	var remotedir = "/home/ubuntu/"
	// upload dir
	var local = "."
	client.Upload(local, remotedir)
	// upload file
	local = "main.go"
	client.Upload(local, remotedir)

}
