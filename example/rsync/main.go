// rsync . ubuntu@118.24.78.141:/home/ubuntu
package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/pytool/ssh"
)

func help() {
	fmt.Println(`参数错误!
	用法: 
		rsync -r dist ubuntu@118.24.78.100:/home/ubuntu`)
}
func main() {
	for idx, args := range os.Args {
		fmt.Println("参数"+strconv.Itoa(idx)+":", args)
	}

	if len(os.Args) != 4 {
		help()
		return
	}
	src := os.Args[2]
	dst := os.Args[3]
	if strings.Contains(src, "@") || !strings.Contains(dst, "@") || !strings.Contains(dst, ":") {
		help()
		return
	}
	// ubuntu@118.24.78.100:/home/ubuntu
	distArr := strings.Split(dst, "@")
	distArr2 := strings.Split(distArr[1], ":")
	client, err := ssh.NewClient(distArr2[0], "22", distArr[0], "")
	if err != nil {
		panic(err)
	}
	defer client.Close()
	var remotedir = distArr2[1]
	client.Upload(src, remotedir)

}
